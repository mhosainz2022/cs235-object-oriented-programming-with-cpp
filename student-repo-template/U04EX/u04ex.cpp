#include <iostream>
using namespace std;

void Program1();
void Program2();
void Program3();
void Program4();
void Program5();
void Program6();
void Program7();

int main()
{
    cout << "Run which program? (1-7): ";
    int prog;
    cin >> prog;

    switch( prog )
    {
        case 1: Program1(); break;
        case 2: Program2(); break;
        case 3: Program3(); break;
        case 4: Program4(); break;
        case 5: Program5(); break;
        case 6: Program6(); break;
        case 7: Program7(); break;
    }

    return 0;
}
